import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ListUsersComponent } from './pages/list-users/list-users.component';
import { PlayersComponent } from './pages/players/players.component';

const routes: Routes = [
  // { path: '/', pathMatch: 'full', redirectTo: 'user-create' },
  { path: 'list-players', component:PlayersComponent},
  { path: 'list-users', component:ListUsersComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
