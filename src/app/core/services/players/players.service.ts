import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { environment } from '../../../../environments/environment.development';
import { catchError, retry, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PlayersService {

  constructor( private http:HttpClient) { }
  API = environment.playersAPI;
  getPlayers(){
    return this.http
    .get(this.API + '/players')
    .pipe(retry(1), catchError(this.handleError));
  }

  handleError(error: any){
    let errorMessage = '';
    if (error.error instanceof ErrorEvent){
      errorMessage = error.error.message;
    }
    else{
      errorMessage = `Error code: ${error.status}\nMessage:  ${error.message}`;
    }
    window.alert(errorMessage);
    return throwError(()=>{
      return errorMessage;
    });
  }
}
