import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule, provideClientHydration } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';;
import { NavBarComponent } from './pages/nav-bar/nav-bar.component';
import { provideAnimationsAsync } from '@angular/platform-browser/animations/async';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatToolbarModule} from '@angular/material/toolbar';
import { MatButtonModule} from '@angular/material/button';
import { MatIconModule} from '@angular/material/icon';
import { PlayersComponent } from './pages/players/players.component';
import { MatCardModule} from '@angular/material/card';
import { MatDividerModule} from '@angular/material/divider';
import { HttpClientModule } from '@angular/common/http';
import { MainHeaderComponent } from './pages/main-header/main-header.component';
import { MainSidebarComponent } from './pages/main-sidebar/main-sidebar.component';
import { ContentWrapperComponent } from './pages/content-wrapper/content-wrapper.component';
import { ControlSidebarComponent } from './pages/control-sidebar/control-sidebar.component';
import { MainFooterComponent } from './pages/main-footer/main-footer.component';
import { ListUsersComponent } from './pages/list-users/list-users.component';
@NgModule({
  declarations: [
    AppComponent,
    NavBarComponent,
    PlayersComponent,
    MainHeaderComponent,
    MainSidebarComponent,
    ContentWrapperComponent,
    ControlSidebarComponent,
    MainFooterComponent,
    ListUsersComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    MatSlideToggleModule,
    MatToolbarModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatDividerModule,
    HttpClientModule
  ],

  providers: [
    provideClientHydration(),
    provideAnimationsAsync()
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
